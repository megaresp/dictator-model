# Dictator Game Factors
This document discusses the various top-level attributes that control the game. This document refers to these attributes as factors.
## What is a factor?
A factor is high-level information in one of three broad categories...
1. Input data provided by the user
2. Output data are the metrics that provide information to the player
3. Account data such as the money held by the treasury
### Input data:
The user provides input during a turn that is accepted into the game and changes the underlying state. The resulting change then informs the users next turn. The game retains the last 12 inputs by the user so that trends can be tracked and inform the calculation of output data.

The top-level inputs envisaged for the initial simplified version of the game are...
* Food production
* Infrastructure
* Justice and law enforcement
* Defence
* Health
* Welfare
* National wage
* Tax rate
* Public borrowing

**ETA:** After further thought, we should probably also include Education and R&D as additional top-level factors. We would then have output metrics that provide something akin to experience points. Thus, education has an output metric that improves over time if sufficient investment is made. And that, in turn, allows R&D to be more successful over time. I think this type of mechanism is the way to solve the problem of how to deliver "unintended consequences" as per our earlier discussion.

**ETA:** An ecology or conservation input is another good idea. Especially as it lends itself to unintended consequences. I'm not certain how this would look as I write this. It's more of a gut instinct that we've found a mechanism for introducing negative events in the future that stem from decisions made now that appear to pay off for the player. And that seem random but aren't, which sits better with me than having a hurricane destroy half your crops because Math.random(). If that once in 1,000 turn hurricane happens every 100 turns because of climate change, then yah boo sucks to you the player! You should have taken the hint.

At the start of each turn the player decides how much money to allocate to each of these top-level factors. If there isn't enough money in the treasury the country must borrow it to cover the shortfall.

In the game proper, these top-level inputs will have child inputs that add complexity to the game. But in phase I, we will limit ourselves to these top-level input factors to make it easier to get the game up and running.
### Output data:
Output data are metrics that describe some aspect of the social, economic and/or politic environment in the player's country. These data help the player formulate the next set of inputs. The game's initial output metrics are...
* Inflation
* Crime rate
* Prison population
* Age distribution
* Occupation distribution (includes child, unemployed, retired, self-employed, employed, government employee, sick, disabled)
* Physical health
* Immigration
* Happiness
* Emmigration

**ETA:** The inputs Education and R&D should produce additional output metrics. Perhaps level of education (primary, secondary, under graduate, etc), and some kind of measure of the sophistication of the economy, or perhaps the type of economy. I'm not exactly sure what that would look like at this point.

**ETA:** Also not sure how the climate/ecology metric might occur as I write this. Needs to be something though, and in this case it will need to trigger warnings and somehow increase the liklihood of certain types of natural disaster occurring.

The game will retain this information for the last 12 turns allowing trends to be graphed and averages to be used.
### Account data:
In Phase I this is likely to be limited to...
* Interest rate per turn
* Minimum principle per turn
* Total debt to repay
* National tax take

More account data can be tracked in later phases, but for now this is enough to accept inputs from the user and know whether they're viable or not.
## What happens during a turn?
A turn begins when the player submits the 9 inputs. Output and Account factors are then calculated, and the resulting information is presented to the player at the end of the turn.

During phase I there will be no complicating factors such as natural disasters, military invasions, riots, etc. The purpose of phase I is to get the underlying model working. Specifically, to ensure that sensible things happen given any set of data.

Example 1: If they player ups the tax rate to 100% of income, the game should immediately end as citizens revolt and the player is removed from office.

Example 2: If the player attempts to borrow 50 times GDP, the world markets should refuse the loan.

Example 3: If the player doubles everybody's pay overnight with no corresponding increase in productivity, the price of everything should double. At least, it should appear that something like that has happened given the limited metrics tracked by the game during this phase.

**ETA:** The R&D output metric, or some variation of it, can be used here to feed into a productivity metric. In other words, productivity can be a thing that gets its inputs from education, crime rate, etc. And in turn, it can be used as an input when calculating whether increasing wages should generate inflation. Or more precisely, how much inflation. And if not inflation, then some other similar factor. Point is, improved productivity produces justified wealth rather than artificial wealth through borrowing, or redistributing income in some insane way.
## Adding complexity:
Once we're happy with the gaming engine, or a model of the gaming engine, we can start to add complexity. I suggest we build complexity as child inputs, outputs and accounts. For example...
* Under tax rate we might have child-categories for income tax, import tarrifs, road tax, sales tax, etc.
* Under immigration we might track illegal, legal, family, retired, educated, uneducated, etc.
* Under debt we might track public, private, mortgage, credit card, business, local authority, etc.

This additional complexity should total the top-level parent factor. And child-inputs may be used to help calculate output metrics of factors other that it's immediate parent. For example, the youth crime rate would be used to calculate the overall 'parent' crime rate. But it might also be used to calculate the cost of justice and laerunning the inform the crime rate, but it might also  be used to calculate emmigration (more crime encourages people to leave).
## Enhancing gameplay:
We've discussed ideas for enhancing gameplay through random events. Above the ETA section hints at ways to introduce unintended consequences of actions that otherwise pay off for the user even before we introduce the complexity of child-data.

