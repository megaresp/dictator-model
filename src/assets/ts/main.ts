import { UtLogSep, UtPad, UtPlural, UtRand } from './utils/Utils';
import Wages from './classes/Wages';
import Fuel from './classes/Fuel';
import DebtPrivate from './classes/DebtPrivate';
import Happiness from './classes/Happiness';
import Inflation from './classes/Inflation';
import { AggregateDemand, CountriesNumbers } from './functions/AggregateDemand';


/**********************************************************************************************
    The following code is a proof-of-concept of passing objects to an inflation object
    and having the inflation object make use of the objects passed to it to calculate
    inflation. As long as a new object implements the iInflation interface, it can be
    passed to the inflation object and used as part of the inflation calculation without
    the need for additional coding in the Infation class.
**********************************************************************************************/
/*

const wages = new Wages();
const fuel = new Fuel();
const debtPrivate = new DebtPrivate();
const happiness = new Happiness();
const inflation = new Inflation();

let newWage: number;
let newFuelPrice: number;
let newDebtPrivate: number;
let newHappiness: number;
const iterations: number = 25;

UtLogSep();
console.log(`Inflation at start: ${inflation.get('current')}%`);
UtLogSep();
for (let i = 0; i < iterations; i++) {
  console.log(`TURN ${ i + 1}:`);
  // Wage changes between -25 and 25 per 'turn'
  newWage = wages.history[wages.history.length - 1] + UtRand(-50, 25);

  // Fuel price changes between -0.6 and 0.06 per 'turn'
  newFuelPrice = fuel.history[fuel.history.length - 1] + UtRand(-12, 6) / 100;

  // Debt changes between -50 and 50 per 'turn'
  newDebtPrivate = debtPrivate.history[debtPrivate.history.length - 1] + UtRand(-100, 25);

  // Happiness changes between -0.2 and 0.02 per 'turn'
  newHappiness = happiness.history[happiness.history.length - 1] + UtRand(-4, 2) / 10;

  console.log(
    `Wage / Fuel: $${newWage.toLocaleString()} / $${newFuelPrice.toLocaleString()}.`
  );
  console.log(`Private Debt: $${newDebtPrivate.toLocaleString()} (${debtPrivate.inflation().toFixed(2)}%).`);
  console.log(`Happiness: ${newHappiness.toFixed(2).toLocaleString()}% (${happiness.inflation().toFixed(2)}%).`);
  wages.update(newWage);
  fuel.update(newFuelPrice);
  
  // console.log(UtRand(-50, 26));

  inflation.update([wages, fuel, debtPrivate, happiness]);

  UtLogSep();
}

console.log(`Inflation at end: ${inflation.get('current')}%`);
UtLogSep();
*/
/**********************************************************************************************
    The above code is a proof-of-concept of passing objects to an inflation object
    and having the inflation object make use of the objects passed to it to calculate
    inflation. As long as a new object implements the iInflation interface, it can be
    passed to the inflation object and used as part of the inflation calculation without
    the need for additional coding in the Infation class.
**********************************************************************************************/




/**********************************************************************************************
    The following code turns Dan's work on the agregate demand formula as described here...
    https://www.thebalance.com/aggregate-demand-definition-formula-components-3305703

    ...into exportable JavaScript modules. Note the import statement(s) at the top of this
    page, and the resulting use of the functions being imported.
**********************************************************************************************/

// function AggregateDemand (
//   consumption: number,
//   investment: number,
//   spending: number,
//   exports: number,
//   imports: number
//   ): number { 
//   return consumption + investment + spending + exports - imports;
// }

// function generate_volatility (volatility: number): number {
//   const min = -volatility;
//   const max = volatility;
//   const grn = Math.random() * (max - min) + min;
//   return Math.round((grn + Number.EPSILON) * 100) / 100;
// };

// function generate_numbers (
//   years: number,
//   starting: number,
//   annual_growth: number,
//   volatility: number): Array<number> {
//   const gns = [starting];
//   let new_number = starting;
//   let random_volatility = generate_volatility(volatility);

//   for (let i = 0; i < (years-1); i++) {
//     random_volatility = generate_volatility(volatility);
//     new_number = new_number + annual_growth + random_volatility;
//     gns.push(new_number);
//   }

//   return gns;
// }

// function countries_numbers (years: number): Array<Array<number>> {
//   const consumption = generate_numbers(years, 15, 0.2, 0.3);
//   const investment = generate_numbers(years, 4, 0.2, 0.3);
//   const spending = generate_numbers(years, 3, 0.05, 0.02);
//   const xports = generate_numbers(years, 2, 0.05, 0.02);
//   const imports = generate_numbers(years, 1, 0.03,0.01);
//   return [consumption, investment, spending, xports, imports];
// }

const years = 25;
const cn = CountriesNumbers (years);

UtLogSep();
console.log(`Modelling agregate demand over ${years} year${UtPlural(years)}...`);
UtLogSep();

for (let i = 0; i < years; i++) {
  let gdp = AggregateDemand(cn[0][i] ,cn[1][i],cn[2][i] ,cn[3][i] ,cn[4][i]);
  console.log(`Year ${UtPad(i + 1, 3)}: $${gdp.toFixed(2)} billion`);
}

UtLogSep();

/**********************************************************************************************
    The above code turns Dan's work on the agregate demand formula as described here...
    https://www.thebalance.com/aggregate-demand-definition-formula-components-3305703

    ...into exportable JavaScript modules. Note the import statement(s) at the top of this
    page, and the resulting use of the functions being imported.
**********************************************************************************************/