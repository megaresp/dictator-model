/**
 * @returns the average wage for a 3 month period starting from item.
 * @throws if item is more than 3 elements from the end of the array.
 */
export const UtAvg3MonthsFrom = (item: string|number = "start", arr: number[]): number => {
  // if (!Array.isArray(arr)) {
  //   throw new Error(
  //     `The second parameter of UtAvg3MonthsFrom() must be an array.`
  //   );
  // }

  const len: number = arr.length;
  if (len < 1) {
    throw new Error(
      `The array supplied to UtAvg3MonthsFrom() must contain a minimum of 3 elements`
    );
  }

  if (typeof item === "string" && item.toLowerCase() === "start") {
    item = 0;
  }
  if (typeof item === "string" && item.toLowerCase() === "end") {
    item = len - 3;
  }

  item = Number(item);

  if (Number.isInteger(item) && item > len - 3) {
    throw new Error(
      `The maximum value of the 'item' parameter accepted by UtAvg3MonthsFrom() is ${
        len - 3
      }.`
    );
  }

  return (arr[item] + arr[item + 1] + arr[item + 2]) / 3;
};

/**
 * Logs a string containing len instances of chr
 */
export const UtLogSep = (len: number = 50, chr: string = "-"): void => {
  console.log(chr.repeat(len));
};

/**
 * Returns value with leading zeros
 */
export const UtPad = (inputNum: number, size: number): string => {
  let num = inputNum.toString();

  while (num.length < size) {
    num = "0" + num;
  }

  return num;
}

/**
 * Returns '' if value = 1, and 's' otherwise
 */
export const UtPlural = (value: number): string  => {
  return value === 1 ? '' : 's';
}

/**
 * @returns a random number in the range from - to
 */
export const UtRand = (from: number, to: number): number => {
  return Math.floor(Math.random() * from) + to;
};
