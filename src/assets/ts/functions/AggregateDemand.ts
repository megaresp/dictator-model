const GenerateVolatility = (volatility: number): number => {
  const min = -volatility;
  const max = volatility;
  const grn = Math.random() * (max - min) + min;
  return Math.round((grn + Number.EPSILON) * 100) / 100;
};

const GenerateNumbers = (
  years: number,
  starting: number,
  annual_growth: number,
  volatility: number
): Array<number> => {
  const gns = [starting];
  let new_number = starting;
  let random_volatility = GenerateVolatility(volatility);

  for (let i = 0; i < (years-1); i++) {
    random_volatility = GenerateVolatility(volatility);
    new_number = new_number + annual_growth + random_volatility;
    gns.push(new_number);
  }

  return gns;
}

export const CountriesNumbers = (years: number): Array<Array<number>> => {
  const consumption = GenerateNumbers(years, 15, 0.2, 0.3);
  const investment = GenerateNumbers(years, 4, 0.2, 0.3);
  const spending = GenerateNumbers(years, 3, 0.05, 0.02);
  const xports = GenerateNumbers(years, 2, 0.05, 0.02);
  const imports = GenerateNumbers(years, 1, 0.03,0.01);
  return [consumption, investment, spending, xports, imports];
}

export const AggregateDemand = (
  consumption: number,
  investment: number,
  spending: number,
  exports: number,
  imports: number
): number => { 
  return consumption + investment + spending + exports - imports;
}