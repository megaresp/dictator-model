import { iInflation } from '../interfaces/Objects';
import { UtAvg3MonthsFrom } from '../utils/Utils';

export default class DebtPrivate implements iInflation {
  /**
   * The 12 elements in this array represent the total private debt over the last 12 turns.
   */
  history: number[] = [
    9038227, 9182758, 9253284, 9397002, 9501667, 9638272, 9714157, 9888684, 9991106, 10032747, 10287111, 10535342,
  ];
  weight = { inflation: 0.05 }; // Private debt inflation contributes 1/20th of the overall inflation rate

  /**
   * @returns The average percentage of wage inflation over the last 6
   *   months by subtracting the most recent 3 months from the previous
   *   3 months and dividing by the most recent 3 months. Then returns
   *   the result multipled by 100 to return as a percent.
   */
  inflation = (): number => {
    // console.log(
    //   `UtAvg3MonthsFrom("end", this.history): ${UtAvg3MonthsFrom(
    //     "end",
    //     this.history
    //   )}`
    // );
    // console.log(
    //   `UtAvg3MonthsFrom(this.history.length - 6, this.history): ${UtAvg3MonthsFrom("end", this.history)}`
    // );

    return (
      ((UtAvg3MonthsFrom('end', this.history) -
        UtAvg3MonthsFrom(this.history.length - 6, this.history)) /
        UtAvg3MonthsFrom('end', this.history)) *
      100
    );
  };

  /**
   * Adds newFuelPrice to the end of this.history
   */
  update = (newDebt: number): void => {
    this.history.shift();
    this.history[this.history.length] = newDebt;
  };
}
