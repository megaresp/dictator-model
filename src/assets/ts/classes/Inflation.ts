import { iInflation } from "../interfaces/Objects";

export default class Inflation {
  /**
   * The 12 elements in this array represent the rate of inflation
   * as a percentage over the last 12 turns.
   */
  history: number[] = [
    1.01, 1.012, 1.008, 1.016, 1.021, 1.028, 1.039, 1.053, 1.043, 1.048, 1.041,
    1.037,
  ];

  /**
   * @returns this.history or this.history[last_element] if item = "current"
   */
  get(item: string|number = 'current'): number|number[] {
    switch (item) {
      case 'current':
        return this.history[this.history.length - 1];
        break;
      case 'first':
        return this.history[0];
        break;
    }

    item = Number(item);

    if (item < 0 || item >= this.history.length) {
      return this.history;
    }

    return this.history[item];
  }

  update(props: iInflation[]): void {
    let newInflation: number = this.history[this.history.length - 1];
    // console.log(`Old inflation: ${newInflation}% ${typeof newInflation}`);

    for (const prop in props) {
      // console.log(props[prop]);
      const propInflation = props[prop].inflation();
      // console.log(`${prop} inflation: ${propInflation}`);
      // console.log(`${prop} weight: ${props[prop].weight}`);
      // console.log(
      //   `Calc: ${propInflation} * ${props[prop].weight} = ${
      //     propInflation * props[prop].weight
      //   }`
      // );
      // console.log(
      //   `Sub total: ${newInflation + (propInflation * props[prop].weight)}`
      // );
      newInflation += propInflation * props[prop].weight.inflation;
    }

    // console.log(`New inflation: ${newInflation}% ${typeof newInflation}`);

    this.history.shift();
    this.history[this.history.length] = Number(newInflation.toFixed(3));
    // console.log(this.history);
  }
}
