import { iInflation, iSocial } from '../interfaces/Objects';
import { UtAvg3MonthsFrom } from '../utils/Utils';

export default class Wages implements iInflation, iSocial {
  /**
   * The 12 elements in this array represent the average wage over the
   * last 12 turns.
   */
  history: number[] = [
    1776, 1785, 1789, 1801, 1792, 1766, 1725, 1773, 1782, 1799, 1801, 1792,
  ];
  weight = { inflation: 0.333, health: 0.25 };

  /**
   * @returns The average percentage of wage inflation over the last 6
   *   months by subtracting the most recent 3 months from the previous
   *   3 months and dividing by the most recent 3 months. Then returns
   *   the result multipled by 100 to return as a percent.
   * @note This method is used for speed of calculation, and to simulate
   *   the delay of wage inflation on the national rate of inflation.
   */
  inflation = (): number => {
    // console.log(
    //   `UtAvg3MonthsFrom("end", this.history): ${UtAvg3MonthsFrom(
    //     "end",
    //     this.history
    //   )}`
    // );
    // console.log(
    //   `UtAvg3MonthsFrom(this.history.length - 6, this.history): ${UtAvg3MonthsFrom("end", this.history)}`
    // );

    return (
      ((UtAvg3MonthsFrom('end', this.history) -
        UtAvg3MonthsFrom(this.history.length - 6, this.history)) /
        UtAvg3MonthsFrom('end', this.history)) *
      100
    );
  };

  /**
   * Adds newWage to the end of this.history
   */
  update = (newWage: number): void => {
    this.history.shift();
    this.history[this.history.length] = newWage;
  };
}
