import { iInflation, iSocial } from '../interfaces/Objects';
import { UtAvg3MonthsFrom } from '../utils/Utils';

export default class Happiness implements iInflation, iSocial {
  /**
   * The 12 elements in this array represent the percentage of people who
   * say they're happy over the last 12 turns.
   */
  history: number[] = [
    0.493, 0.517, 0.522, 0.515, 0.503, 0.501, 0.498, 0.483, 0.489, 0.501, 0.502, 0.501,
  ];

/**
 * The impact of happiness on the inflation rate is 2.5%
 * The impact of happiness on national health is 10%
 */
  weight = { inflation: 0.025, health: 0.1 }

  /**
   * @returns The average impact of happiness on inflation over the last 6
   *   months by subtracting the most recent 3 months from the previous
   *   3 months and dividing by the most recent 3 months. Then returns
   *   the result multipled by 100 to return as a percent.
   */
  inflation = (): number => {
    return (
      ((UtAvg3MonthsFrom('end', this.history) -
        UtAvg3MonthsFrom(this.history.length - 6, this.history)) /
        UtAvg3MonthsFrom('end', this.history)) *
      100
    );
  }

  /**
   * Adds newWage to the end of this.history
   */
  update = (newHappiness: number): void => {
    this.history.shift();
    this.history[this.history.length] = newHappiness;
  };
}
