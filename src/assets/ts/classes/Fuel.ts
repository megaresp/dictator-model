import { iInflation } from '../interfaces/Objects';
import { UtAvg3MonthsFrom } from '../utils/Utils';

export default class Fuel implements iInflation {
  /**
   * The 12 elements in this array represent the average price per
   * litre of fuel over the last 12 turns.
   */
  history: number[] = [
    2.27, 2.28, 2.29, 2.32, 2.33, 2.35, 2.41, 2.4, 2.41, 2.41, 2.42, 2.43,
  ];
  weight = { inflation: 0.125 }; // Fuel price inflation contributes 1/8th of overall inflation rate

  /**
   * @returns The average percentage of wage inflation over the last 6
   *   months by subtracting the most recent 3 months from the previous
   *   3 months and dividing by the most recent 3 months. Then returns
   *   the result multipled by 100 to return as a percent.
   */
  inflation = (): number => {
    // console.log(
    //   `UtAvg3MonthsFrom("end", this.history): ${UtAvg3MonthsFrom(
    //     "end",
    //     this.history
    //   )}`
    // );
    // console.log(
    //   `UtAvg3MonthsFrom(this.history.length - 6, this.history): ${UtAvg3MonthsFrom("end", this.history)}`
    // );

    return (
      ((UtAvg3MonthsFrom('end', this.history) -
        UtAvg3MonthsFrom(this.history.length - 6, this.history)) /
        UtAvg3MonthsFrom('end', this.history)) *
      100
    );
  };

  /**
   * Adds newFuelPrice to the end of this.history
   */
  update = (newFuelPrice: number): void => {
    this.history.shift();
    this.history[this.history.length] = newFuelPrice;
  };
}
