interface iBase {
  history: number[];
  weight: {};
  update(newDebt: number): void;
}

export interface iInflation extends iBase {
  // history: number[];
  weight: { inflation: number };
  inflation(): number;
  // update(newDebt: number): void;
}

export interface iSocial {
  // history: number[];
  weight: { health: number };
  // update(newDebt: number): void;
}