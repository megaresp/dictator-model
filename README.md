# Dictator Modelling System
A means of modelling the impact of different game elements against each other to fine-tune the underlying game system.
## Developer Notes

To install this project locally...
* Make sure you're **logged in** to your GitLab account
* ```git clone https://gitlab.com/megaresp/dictator-model.git```
* ```cd dictator-model```
* ```npm install```

## Build the Dev environment
* ```npm run build```

## Run the Modeller in a browser
* ```npm run dev```
* [http://localhost:1234](http://localhost:1234)

## Working on the codebase
* Start working on files in ```~/src/```